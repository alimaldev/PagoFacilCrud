<?php

namespace PagoFacilBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class empleadosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, ['required' => true])
            ->add('apellidoPaterno', TextType::class, ['required' => true])
            ->add('apellidoMaterno', TextType::class, ['required' => true])
            ->add('fechaNacimiento', 'date')
            ->add('salarioAnual')
        ;
    }
}
