<?php

namespace PagoFacilBundle\Controller;

use PagoFacilBundle\Entity\datos_empleados;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use PagoFacilBundle\Entity\empleados;
use PagoFacilBundle\Form\empleadosType;

/**
 * empleados controller.
 *
 * @Route("/empleados")
 */
class empleadosController extends Controller
{
    /**
     * Lists all empleados entities.
     *
     * @Route("/", name="empleados_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $query = 'SELECT e.id, e.nombre, e.apellidoPaterno, e.apellidoMaterno, de.fechaNacimiento, de.salarioAnual
                  FROM PagoFacilBundle:empleados e
                  JOIN PagoFacilBundle:datos_empleados de
                  WHERE e.id = de.id';

        $empleados = $this->getDoctrine()->getEntityManager()
            ->createQuery($query)
            ->getResult();

        return $this->render('empleados/index.html.twig', array(
            'empleados' => $empleados,
        ));
    }

    /**
     * Creates a new empleados entity.
     *
     * @Route("/new", name="empleados_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $empleado = new empleados();
        $datosEmpleados = new datos_empleados();
        $form = $this->createForm(new empleadosType(), []);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $dataFormEmpleado = $form->getData();

            $empleado->setNombre($dataFormEmpleado['nombre']);
            $empleado->setApellidoPaterno($dataFormEmpleado['apellidoPaterno']);
            $empleado->setApellidoMaterno($dataFormEmpleado['apellidoMaterno']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($empleado);
            $em->flush();

            $datosEmpleados->setIdEmpleado($empleado->getId());
            $datosEmpleados->setFechaNacimiento($dataFormEmpleado['fechaNacimiento']);
            $datosEmpleados->setSalarioAnual($dataFormEmpleado['salarioAnual']);

            $em->persist($datosEmpleados);
            $em->flush();

            return $this->redirectToRoute('empleados_show', array('id' => $empleado->getId()));
        }

        return $this->render('empleados/new.html.twig', array(
            'empleado' => $empleado,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a empleados entity.
     *
     * @Route("/{id}/api", name="empleados_show_api")
     * @Method("GET")
     * @param empleados       $empleado
     * @param datos_empleados $datos_empleados
     *
     * @return JsonResponse
     */
    public function showApiAction(empleados $empleado, datos_empleados $datos_empleados)
    {
        $response = [
            'id' => $empleado->getId(),
            'Nombre' => $empleado->getNombre(),
            'apellido Paterno' => $empleado->getApellidoPaterno(),
            'apellido Materno' => $empleado->getApellidoMaterno(),
            'fecha de Nacimiento' => $datos_empleados->getFechaNacimiento()->format('d-m-Y'),
            'salario' => $datos_empleados->getSalarioAnual(),
        ];

        return new JsonResponse($response);
    }

    /**
     * Finds and displays a empleados entity.
     *
     * @Route("/{id}", name="empleados_show")
     * @Method("GET")
     */
    public function showAction(empleados $empleado)
    {
        $deleteForm = $this->createDeleteForm($empleado);

        return $this->render('empleados/show.html.twig', array(
            'empleado' => $empleado,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a empleados entity.
     *
     * @Route("/{id}", name="empleados_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, empleados $empleado)
    {
        $form = $this->createDeleteForm($empleado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($empleado);
            $em->flush();
        }

        return $this->redirectToRoute('empleados_index');
    }

    /**
     * Creates a form to delete a empleados entity.
     *
     * @param empleados $empleado The empleados entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(empleados $empleado)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('empleados_delete', array('id' => $empleado->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
