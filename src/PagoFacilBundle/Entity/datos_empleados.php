<?php

namespace PagoFacilBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * datos_empleados
 *
 * @ORM\Table(name="datos_empleados")
 * @ORM\Entity(repositoryClass="PagoFacilBundle\Repository\datos_empleadosRepository")
 */
class datos_empleados
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_empleado", type="integer", nullable=true)
     */
    private $idEmpleado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_nacimiento", type="date")
     */
    private $fechaNacimiento;

    /**
     * @var int
     *
     * @ORM\Column(name="salario_anual", type="integer")
     */
    private $salarioAnual;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmpleado
     *
     * @param integer $idEmpleado
     *
     * @return datos_empleados
     */
    public function setIdEmpleado($idEmpleado)
    {
        $this->idEmpleado = $idEmpleado;

        return $this;
    }

    /**
     * Get idEmpleado
     *
     * @return int
     */
    public function getIdEmpleado()
    {
        return $this->idEmpleado;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     *
     * @return datos_empleados
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set salarioAnual
     *
     * @param integer $salarioAnual
     *
     * @return datos_empleados
     */
    public function setSalarioAnual($salarioAnual)
    {
        $this->salarioAnual = $salarioAnual;

        return $this;
    }

    /**
     * Get salarioAnual
     *
     * @return int
     */
    public function getSalarioAnual()
    {
        return $this->salarioAnual;
    }
}
